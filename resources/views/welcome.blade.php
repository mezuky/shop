@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Despre proiect</div>
                <div class="panel-body">
                    <strong>Autor: </strong>Alexandru Bugărin
                    <br>
                    <strong>Contact: </strong><a href="mailto:alexandru.bugarin@gmail.com">alexandru.bugarin@gmail.com</a>
                    <br>
                    <strong>Codul sursă: </strong><a target="_blank" href="https://gitlab.com/mezuky/shop">Shop</a>
                    <br>
                    <strong>Descriere: </strong>Proiectul a fost creat pentru cursul <strong>Algoritmi și Structuri de Date 2</strong>. Aplicația are ca scop gestionarea produselor unui magazin.
                    <br>
                    <strong>Funcții disponibile:</strong>
                    <ul>
                        <li>Crearea unui cont</li>
                        <li>Accesarea aplicației cu contul creat</li>
                        <li>Afșiarea tuturor produselor</li>
                        <li>Afșiarea produselor valide</li>
                        <li>Afșiarea produselor expirate</li>
                        <li>Căutare (dupa cod sau nume) în toate produsele</li>
                        <li>Căutare (dupa cod sau nume) în produsele expirate</li>
                        <li>Editarea unui produs</li>
                        <li>Adăugarea unui nou produs</li>
                        <li>Ștergerea tuturor produselor expirate</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
