@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['route' => 'products.store']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Nume produs') !!}
                        {!! Form::text('name', null, ['placeholder' => 'Smartphone Nexus 5', 'class' => 'form-control']) !!}
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('code', 'Cod produs') !!}
                        {!! Form::text('code', null, ['placeholder' => '01312', 'class' => 'form-control']) !!}
                        @if($errors->has('code'))
                            <span class="text-danger">{{ $errors->first('code') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('price', 'Preț produs') !!}
                        {!! Form::text('price', null, ['placeholder' => '1500', 'class' => 'form-control']) !!}
                        @if($errors->has('price'))
                            <span class="text-danger">{{ $errors->first('price') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('quantity', 'Cantitate produs') !!}
                        {!! Form::text('quantity', null, ['placeholder' => '4', 'class' => 'form-control']) !!}
                        @if($errors->has('quantity'))
                            <span class="text-danger">{{ $errors->first('quantity') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('received_at', 'Data primirii') !!}
                        {!! Form::text('received_at', null, ['placeholder' => 'yyyy-mm-dd', 'class' => 'form-control']) !!}
                        @if($errors->has('received_at'))
                            <span class="text-danger">{{ $errors->first('received_at') }}</span>
                        @endif
                    </div>
                    <div class="form-group">
                        {!! Form::label('expires_at', 'Data expirării') !!}
                        {!! Form::text('expires_at', null, ['placeholder' => 'yyyy-mm-dd', 'class' => 'form-control']) !!}
                        @if($errors->has('expires_at'))
                            <span class="text-danger">{{ $errors->first('expires_at') }}</span>
                        @endif
                    </div>
                    {!! Form::submit('Adaugă produs', ['class' => 'btn btn-block btn-info']) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
