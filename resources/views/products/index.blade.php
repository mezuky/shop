@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            @if(count($products) > 0)
                <h4>{{ $text }}</h4>
                <div class="panel panel-default">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nume produs</th>
                                <th>Cod</th>
                                <th>Preț</th>
                                <th>Cantitate</th>
                                <th>Data primirii</th>
                                <th>Data de expirare</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                                <tr>
                                    <td><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->name }}</a></td>
                                    <td><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->code }}</a></td>
                                    <td>{{ $product->price }} lei</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->received_at }}</td>
                                    <td>{{ $product->expires_at }}</td>
                                    <td>
                                        @if($product->expired)
                                            <span class="label label-danger">Expirat</span>
                                        @else
                                            <span class="label label-success">Valid</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <div class="alert alert-warning">Nu există niciun produs.</div>
            @endif
        </div>
    </div>
</div>
@endsection
