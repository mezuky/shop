@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong>Nume produs: </strong>{{ $product->name }}
                </div>
                <div class="panel-body">
                    <div><strong>Preț produs: </strong>{{ $product->price }} lei</div>
                    <div><strong>Cod produs: </strong>{{ $product->code }}</div>
                    <div><strong>Stoc: </strong>{{ $product->quantity }} buc.</div>
                    <div><strong>Data adăugării: </strong>{{ $product->received_at }}</strong></div>
                    <div><strong>Data expirării: </strong>{{ $product->expires_at }}</div>
                </div>
                <div class="panel-footer">
                    <div class="row">
                        <div class="col-xs-6">
                            <a href="{{ route('products.edit', ['product' => $product->id]) }}" class="btn btn-block btn-info">Editează produsul</a>
                        </div>
                        <div class="col-xs-6">
                            {!! Form::open(['route' => ['products.destroy', $product->id], 'method' => 'delete']) !!}
                                {!! Form::submit('Șterge produsul', ['class' => 'btn btn-block btn-danger']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
