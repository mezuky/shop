@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2">
            @if(session('success'))
                <div class="alert alert-success">{{ session('success') }}</div>
            @endif
            <div class="panel panel-default">
                <div class="panel-body">
                    {!! Form::open(['route' => 'search.expired-results']) !!}
                        <div class="form-group">
                            {!! Form::label('term', 'Căutați doar în produsele expirate, după cod sau nume') !!}
                            {!! Form::text('term', null, ['class' => 'form-control']) !!}
                        </div>
                        {!! Form::submit('Căutați', ['class' => 'btn btn-block btn-info']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
            @if(isset($results) && count($results) > 0)
                <div class="panel panel-default">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nume produs</th>
                                <th>Cod</th>
                                <th>Preț</th>
                                <th>Cantitate</th>
                                <th>Data primirii</th>
                                <th>Data de expirare</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($results as $product)
                                <tr>
                                    <td><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->name }}</a></td>
                                    <td><a href="{{ route('products.show', ['product' => $product->id]) }}">{{ $product->code }}</a></td>
                                    <td>{{ $product->price }} lei</td>
                                    <td>{{ $product->quantity }}</td>
                                    <td>{{ $product->received_at }}</td>
                                    <td>{{ $product->expires_at }}</td>
                                    <td>
                                        @if($product->expired)
                                            <span class="label label-danger">Expirat</span>
                                        @else
                                            <span class="label label-success">Valid</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </div>
                </table>
            @elseif(isset($term) && count($results) < 1)
                <div class="alert alert-danger">Nu a fost găsit niciun produs.</div>
            @endif
        </div>
    </div>
</div>
@endsection
