<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'code',
        'quantity',
        'received_at',
        'expires_at',
    ];

    protected $appends = [
        'expired',
    ];

    public function scopeExpired($query)
    {
        return $query->where('expires_at', '<', date('Y-m-d'));
    }

    public function scopeValid($query)
    {
        return $query->where('expires_at', '>=', date('Y-m-d'));
    }

    public function getExpiredAttribute()
    {
        return $this->expires_at < date('Y-m-d');
    }
}
