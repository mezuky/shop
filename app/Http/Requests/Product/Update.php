<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Update extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:3,100',
            'code' => [
                'required',
                'digits:5',
                Rule::unique('products')->ignore($this->code, 'code'),
            ],
            'price' => 'required|numeric|between:0,99999',
            'quantity' => 'required|numeric|between:1,1000',
            'received_at' => 'required|date',
            'expires_at' => 'required|date',
        ];
    }
}
