<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class SearchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function allIndex()
    {
        return view('search.all');
    }

    public function expiredIndex()
    {
        return view('search.expired');
    }

    public function allResults(Request $request)
    {
        $term = $request->get('term') . '%';
        $results = Product::where('code', 'like', $term)
            ->orWhere('name', 'like', $term)
            ->orderBy('name', 'asc')
            ->get()
        ;

        return view('search.all', compact('results', 'term'));
    }

    public function expiredResults(Request $request)
    {
        $term = $request->get('term') . '%';
        $results = Product::where('code', 'like', $term)
            ->orWhere('name', 'like', $term)
            ->expired()
            ->orderBy('name', 'asc')
            ->get()
        ;

        return view('search.expired', compact('results', 'term'));
    }
}
