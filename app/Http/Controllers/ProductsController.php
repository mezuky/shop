<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests\Product\Store;
use App\Http\Requests\Product\Update;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('name', 'asc')->get();
        $text = 'Toate produsele';
        return view('products.index', compact('products', 'text'));
    }

    public function expired()
    {
        $products = Product::orderBy('name', 'asc')->expired()->get();
        $text = 'Produsele expirate';
        return view('products.index', compact('products', 'text'));
    }

    public function valid()
    {
        $products = Product::orderBy('name', 'asc')->valid()->get();
        $text = 'Produsele valide';
        return view('products.index', compact('products', 'text'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Store $request)
    {
        $product = new Product();
        $product->fill($request->only(['name', 'code', 'price', 'quantity', 'received_at', 'expires_at']));
        $product->save();

        return redirect()->route('products.index')->with('success', 'Produsul a fost adăugat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Update $request, Product $product)
    {
        $product->fill($request->only(['name', 'price', 'code', 'quantity', 'expires_at', 'received_at']));
        $product->save();

        return redirect()->route('products.show', $product->id)->with('success', 'Produsul a fost actualizat.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->route('products.index')->with('success', 'Produsul a fost șters.');
    }

    public function destroyExpired()
    {
        Product::expired()->delete();
        return redirect()->route('products.expired');
    }
}
