<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductsTableSeeder extends Seeder
{
    private $products = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        for($i = 1; $i <= $this->products; $i++) {
            $product = new Product();
            $product->name = $faker->sentence(2);
            $product->code = $this->uniqueCode();
            $product->price = rand(5,999);
            $product->quantity = rand(1, 44);
            $product->received_at = date('Y-m-d');
            $product->expires_at = $this->expiresAt();
            $product->save();
        }
    }

    private function uniqueCode()
    {
        do {
            $code = str_shuffle('1234567890');
            $code = substr($code, 0, 5);
        } while (Product::where('code', $code)->count());

        return $code;
    }

    private function expiresAt()
    {
        $expired = rand(0, 1);
        if ($expired) {
            return date('Y-m-d', strtotime('-' . rand(1, 300) . ' days'));
        }

        return date('Y-m-d', strtotime('+' . rand(1, 300) . ' days'));
    }
}
