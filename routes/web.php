<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'WelcomeController@index');

Auth::routes();

Route::get('products/valid', 'ProductsController@valid')->name('products.valid');
Route::get('products/expired', 'ProductsController@expired')->name('products.expired');
Route::get('products/expired/delete', 'ProductsController@destroyExpired')->name('products.expired.delete');
Route::resource('products', 'ProductsController');
Route::get('/search/all', 'SearchController@allIndex')->name('search.all');
Route::get('/search/expired', 'SearchController@expiredIndex')->name('search.expired');
Route::post('/search/all/results', 'SearchController@allResults')->name('search.all-results');
Route::post('/search/expired/results', 'SearchController@expiredResults')->name('search.expired-results');
